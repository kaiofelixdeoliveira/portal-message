import { Routes } from "@angular/router";
import { CampaignComponent } from "src/app/pages/campaign/campaign.component";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { MessageComponent } from "../../pages/message/message.component";

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "message", component: MessageComponent },
  { path: "campaign", component: CampaignComponent },
 
];
