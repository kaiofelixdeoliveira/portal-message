import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { MessageComponent } from "../../pages/message/message.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ModalModule } from "ngx-bootstrap/modal";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { CampaignComponent } from "src/app/pages/campaign/campaign.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    Ng2SearchPipeModule,

  ],
  declarations: [
    DashboardComponent,
    MessageComponent,
    CampaignComponent]

})
export class AdminLayoutModule { }
