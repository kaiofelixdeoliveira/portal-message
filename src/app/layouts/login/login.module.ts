import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ReactiveFormsModule } from '@angular/forms';
import { LoginRoutes } from './login-routing.module';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginRoutes),
        ReactiveFormsModule,
        
    ],
    declarations: []
})
export class LoginModule {}
