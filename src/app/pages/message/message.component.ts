import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { AfterViewInit, ChangeDetectorRef, Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { Approval } from "src/app/models/approval-model";
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Message } from 'src/app/models/message-model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApprovalService } from 'src/app/service/approval.service';
import { first } from 'rxjs';
import { MessageService } from 'src/app/service/message.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Campaign } from 'src/app/models/campaign-model';
import { CampaignService } from 'src/app/service/campaign.service';



defineLocale('pt-br', ptBrLocale);


@Component({
  selector: "app-message",
  templateUrl: "message.component.html"
})
export class MessageComponent implements OnInit {
  constructor(private approvalService: ApprovalService,
    private messageService: MessageService,
    private campaignService: CampaignService,
    private cdRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService,
    private modalService: NgbModal
  ) {

  }
  focusDateApproval;
  focusDateSend;
  focusDateTrigger
  focusDescription;
  focusNameCamp;
  focusApproval;
  focusMessage;
  focusPhone;
  locale = 'pt-br';
  listApproval: Approval[] = [];
  editLine: Message;
  submittedEdit = false;
  formEdit: FormGroup;
  messages: Message[] = [];
  campaigns: Campaign[] = [];
  valueSelectedDropDownCampaign = '';
  searchText;
  changeSort = false;
  NOTHING_CAMPAIGN_NAME = 'empty';
  NOTHING_CAMPAIGN_TYPE = 'empty';
  NOTHING_CAMPAIGN_CODE = 4


  public isCollapsed = false;

  closeResult: string;
  @ViewChild('modalEdit') public modalEdit: ModalDirective;
  @ViewChild('modalDetails') public modalDetails: ModalDirective;
  @ViewChild('modalSendMessage') public modalSendMessage: ModalDirective;
  @ViewChild('modalConfirmApproval') public modalConfirmApproval: ModalDirective;
  @ViewChild('modalDisapproval') public modalDisapproval: ModalDirective;

  ngOnInit() {
    this.getAllMessages();
    this.getAllCampaign();
    this.formEdit = this.formBuilder.group({
      phone: ['', Validators.required],
      description: ['', Validators.required],
      message: ['', Validators.required],
      approver: ['', Validators.required],
      isApproval: ['', Validators.required],
      isSend: ['', Validators.required],
      dateTrigger: ['', Validators.required],
      dateSend: ['', Validators.required],
      dateApproval: ['', Validators.required],

    });
    this.localeService.use(this.locale);
    this.editLine = { id: null, dateSend: '', dateTrigger: '', description: '', phone: '', message: '', isSend: false, campaign: new Campaign(), approval: new Approval() };
  }


  open(content) {
    this.modalService.open(content, { windowClass: 'modal-search' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  formatDate(date: string) {
    if (date && !String(date).includes("/")) {
      let newDate = new Date(date);
      return newDate.toLocaleDateString(this.locale);
    } else {
      return date;
    }
  }

  update() {

    let message: any = {};
    message.id = this.editLine.id;
    message.phone = this.formEdit.controls.phone.value;
    message.description = this.formEdit.controls.description.value;
    message.message = this.formEdit.controls.message.value;
    message.send = this.formEdit.controls.isSend.value;
    message.dateTrigger = this.formatDate(this.formEdit.controls.dateTrigger.value);
    message.dateSend = this.formatDate(this.formEdit.controls.dateSend.value);

    let campaign: any = {
      id: this.editLine.campaign.id,
      name: this.editLine.campaign.name === 'Nenhum' ? this.NOTHING_CAMPAIGN_NAME : this.editLine.campaign.name,
      type: this.editLine.campaign.type
    }
    let approval: any =
    {
      id: this.editLine.approval.id,
      approver: this.formEdit.controls.approver.value,
      dateApproval: this.formatDate(this.formEdit.controls.dateApproval.value),
      approval: this.formEdit.controls.isApproval.value,

    };

    message.campaign = campaign;
    message.approval = approval;

    this.modalEdit.hide();
    console.log(message)
    this.messageService.updateMessage(message).subscribe(() => {
      this.getAllMessages();

    });

  }

  sendMessage() {

    let messageUpdate: any = {};
    messageUpdate.id = this.editLine.id;
    this.modalSendMessage.hide();

    this.messageService.sendMessage(messageUpdate).subscribe(() => {
      this.getAllMessages();

    });

  }




  chargerEdit(id: number) {
    if (id) {
      let itemIndex = this.messages.findIndex(item => item.id == id);
      this.editLine = this.messages[itemIndex];
      this.formEdit.patchValue({
        phone: this.editLine['phone'],
        description: this.editLine['description'],
        message: this.editLine['message'],
        approver: this.editLine.approval['approver'],
        isApproval: this.editLine.approval['isApproval'],
        isSend: this.editLine['isSend'],
        dateTrigger: this.editLine['dateTrigger'],
        dateSend: this.editLine['dateSend'],
        dateApproval: this.editLine.approval['dateApproval'],
      });
      let name = this.editLine.campaign.name;
      this.valueSelectedDropDownCampaign = name === this.NOTHING_CAMPAIGN_NAME ? 'Nenhum' : name;

    }
  }
  closeModalEdit() {
    this.getAllMessages();
    this.modalEdit.hide();


  }
  showModalEdit(id: number) {
    this.chargerEdit(id);
    this.modalEdit.show();

  }
  showModalDetails(id: number) {
    if (id) {
      this.modalDetails.show();
      let itemIndex = this.messages.findIndex(item => item.id == id);
      this.editLine = this.messages[itemIndex];
      this.valueSelectedDropDownCampaign = this.editLine.campaign.name;


    }
  }

  showModalApproval(id: number) {
    if (id) {
      this.modalConfirmApproval.show();
      let itemIndex = this.messages.findIndex(item => item.id == id);
      this.editLine = this.messages[itemIndex];

    }
  }

  showModalDisapproval(id: number) {
    if (id) {
      this.modalDisapproval.show();
      let itemIndex = this.messages.findIndex(item => item.id == id);
      this.editLine = this.messages[itemIndex];

    }
  }

  showModalSend(id: number) {
    if (id) {
      this.modalSendMessage.show();
      let itemIndex = this.messages.findIndex(item => item.id == id);
      this.editLine = this.messages[itemIndex];

    }
  }

  getAllMessages() {
    this.messageService.getAllMessages().subscribe((messages) => {
      this.fromPayload(messages);

    });
  }
  fromPayload(list) {


    this.messages = [];
    list.forEach(list => {

      let campaign: Campaign = {
        id: list.campaign.id,
        name: list.campaign.name == this.NOTHING_CAMPAIGN_NAME ? 'Nenhum' : list.campaign.name,
        type: list.campaign.type
      }

      let approval: Approval =
      {
        id: list.approval.id,
        approver: list.approval.approver,
        dateApproval: list.approval.dateApproval,
        isApproval: list.approval.approval,

      };
      let messages: Message = {
        id: list.id,
        description: list.description,
        phone: list.phone,
        message: list.message,
        isSend: list.send,
        campaign: campaign,
        dateSend: list.dateSend,
        dateTrigger: list.dateTrigger,
        approval: approval
      };
      this.messages.push(messages);

    })
  }
  approval(approval: Approval) {
    this.modalConfirmApproval.hide();
    let approvalUpdate = {
      id: approval.id,
      approval: true,
      approver: approval.approver,
      dateApproval: this.formatDate(approval.dateApproval)
    }

    this.approvalService.approval(approvalUpdate).pipe(first()).subscribe(() => {

      this.getAllMessages();

    });
  }

  disapproval(approval: Approval) {
    this.modalDisapproval.hide();
    let approvalUpdate = {
      id: approval.id,
      approval: false,
    }

    this.approvalService.disapproval(approvalUpdate).pipe(first()).subscribe(() => {

      this.getAllMessages();

    });
  }


  getAllCampaign() {
    this.campaignService.getAllCampaign().pipe(first()).subscribe((c) => {
      this.campaigns = c;

    });
  }
  changeValueDrowpDown(value) {
    let campaign: Campaign = {
      id: value.id,
      name: value.name === this.NOTHING_CAMPAIGN_NAME ? 'Nenhum' : value.name,
      type: value.type
    }
    this.valueSelectedDropDownCampaign = campaign.name;
    this.editLine.campaign = campaign;

  }


}



