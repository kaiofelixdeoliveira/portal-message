import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { AfterViewInit, ChangeDetectorRef, Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { Approval } from "src/app/models/approval-model";
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Message } from 'src/app/models/message-model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApprovalService } from 'src/app/service/approval.service';
import { first } from 'rxjs';
import { MessageService } from 'src/app/service/message.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Campaign } from 'src/app/models/campaign-model';
import { CampaignService } from 'src/app/service/campaign.service';



defineLocale('pt-br', ptBrLocale);


@Component({
  selector: "app-campaign",
  templateUrl: "campaign.component.html"
})
export class CampaignComponent implements OnInit {
  constructor(private approvalService: ApprovalService,
    private messageService: MessageService,
    private campaignService: CampaignService,
    private cdRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService,
    private modalService: NgbModal
  ) {

  }
  focusDateApproval;
  focusDateSend;
  focusDateTrigger
  focusDescription;
  focusNameCamp;
  focusApproval;
  focusMessage;
  focusPhone;
  locale = 'pt-br';
  listApproval: Approval[] = [];
  editLine: Message;
  submittedEdit = false;
  formEdit: FormGroup;
  messages: Message[] = [];
  campaigns: Campaign[] = [];
  valueSelectedDropDownCampaign = '';
  searchText;
  changeSort = false;
  NOTHING_CAMPAIGN_NAME = 'empty';
  NOTHING_CAMPAIGN_TYPE = 'empty';
  NOTHING_CAMPAIGN_CODE = 4


  public isCollapsed = false;

  closeResult: string;
  @ViewChild('modalEdit') public modalEdit: ModalDirective;
  @ViewChild('modalDetails') public modalDetails: ModalDirective;
  @ViewChild('modalSendMessage') public modalSendMessage: ModalDirective;
  @ViewChild('modalConfirmApproval') public modalConfirmApproval: ModalDirective;
  @ViewChild('modalDisapproval') public modalDisapproval: ModalDirective;

  ngOnInit() {

    this.getAllCampaign();
  }


  open(content) {
    this.modalService.open(content, { windowClass: 'modal-search' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  getAllCampaign() {
    this.campaignService.getAllCampaign().pipe(first()).subscribe((c) => {
      this.campaigns = c;

    });
  }

}



