import { Approval } from "./approval-model";
import { Campaign } from "./campaign-model";


export class Message {

    constructor(id?: number, phone?: string, description?: string, message?: string, isSend?: boolean, campaign?: Campaign, approval?: Approval, dateSend?: string,
        dateTrigger?: string) {

        this.id = id;
        this.description = description;
        this.phone = phone;
        this.message = message;
        this.isSend = isSend;
        this.campaign = campaign;
        this.approval = approval;
        this.dateSend = dateSend;
        this.dateTrigger = dateTrigger;
    }

    public id: number;
    public phone: string;
    public message: string;
    public isSend: boolean;
    public campaign: Campaign;
    public dateSend: string;
    public dateTrigger: string;
    public approval: Approval;
    public description: string;


}
