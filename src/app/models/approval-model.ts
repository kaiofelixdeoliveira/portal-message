import { Message } from "./message-model";



export class Approval {

    constructor(id?: number,approver?: string, dateApproval?: string, isApproval?: boolean) {


        this.id = id;
       
        this.approver = approver;
        this.isApproval=isApproval;
        this.dateApproval = dateApproval;
        

    }

    public id: number;
   
    public approver: string;
    public isApproval: boolean;
    public dateApproval:string;
   

    


}
