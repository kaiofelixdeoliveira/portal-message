import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry } from 'rxjs';
import { catchError } from 'rxjs';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Approval } from '../models/approval-model';
import { Message } from '../models/message-model';
import { FactoryEndpointService } from './factory-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  endpointUrl: string = "/message";

  constructor(private http: HttpClient, private factoryEndpointService: FactoryEndpointService) {

    
   }
  

  // GET
  getAllMessages(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrlApproval}${this.endpointUrl}/list`,{headers:this.factoryEndpointService.httpOptions.headers})
      .pipe(
        retry(1),
        catchError(this.factoryEndpointService.errorHandler)
      )
  }



// UPDATE
updateMessage(message): Observable<any> {
  return this.http.patch<any>(`${environment.apiUrlApproval}${this.endpointUrl}/update`, JSON.stringify(message), this.factoryEndpointService.httpOptions)
    .pipe(
      retry(1),
      catchError(this.factoryEndpointService.errorHandler)
    )
}

// POST
sendMessage(message): Observable<any> {
  return this.http.post<any>(`${environment.apiUrlApproval}${this.endpointUrl}/send`, JSON.stringify(message), this.factoryEndpointService.httpOptions)
    .pipe(
      retry(1),
      catchError(this.factoryEndpointService.errorHandler)
    )
}
}
