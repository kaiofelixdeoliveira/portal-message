import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { retry } from 'rxjs';
import { catchError } from 'rxjs';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Approval } from '../models/approval-model';
import { Message } from '../models/message-model';
import { FactoryEndpointService } from './factory-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class ApprovalService {

  endpointUrl: string = "/approval";

  constructor(private http: HttpClient, private factoryEndpointService: FactoryEndpointService) { }


  // POST
  approval(data): Observable<Approval> {
    return this.http.patch<Approval>(`${environment.apiUrlApproval}${this.endpointUrl}`, JSON.stringify(data), this.factoryEndpointService.httpOptions)
      .pipe(
        retry(1),
        catchError(this.factoryEndpointService.errorHandler)
      )
  }

  disapproval(data): Observable<Approval> {
    return this.http.patch<Approval>(`${environment.apiUrlApproval}${this.endpointUrl}/disapproval`, JSON.stringify(data), this.factoryEndpointService.httpOptions)
      .pipe(
        retry(1),
        catchError(this.factoryEndpointService.errorHandler)
      )
  }
}
