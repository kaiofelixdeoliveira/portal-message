import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry } from 'rxjs';
import { catchError } from 'rxjs';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Campaign } from '../models/campaign-model';
import { FactoryEndpointService } from './factory-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {


  endpointUrl: string = "/campaign";

  constructor(private http: HttpClient, private factoryEndpointService: FactoryEndpointService) {

    
  }
 

 // GET
 getAllCampaign(): Observable<any[]> {
   return this.http.get<any[]>(`${environment.apiUrlApproval}${this.endpointUrl}/all`,{headers:this.factoryEndpointService.httpOptions.headers})
     .pipe(
       retry(1),
       catchError(this.factoryEndpointService.errorHandler)
     )
 }
}
